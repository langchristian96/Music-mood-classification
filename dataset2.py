import random


class Dataset2:
    data = []
    result =[]
    crt = 0
    def __init__(self):
        filepath = "C:\work\licenta\\ro\\ubb\\classification2.txt"
        with open(filepath, "r") as f:
            lineargs = f.readline().strip().split(",")
            for i in range(280):
                line = []
                if(len(lineargs)!=510275):
                    continue
                for i in range(len(lineargs)-1):
                    line.append(float(lineargs[i]))
                resline = self.hot(int(lineargs[len(lineargs)-1]))
                self.data.append(line)
                self.result.append(resline)
                lineargs = f.readline().strip().split(",")
            random.shuffle(self.data)
        # print(self.data)
        #
        # print(self.result)

    def hot(self, i):
        res = [0, 0, 0, 0]
        res[i] = 1
        return res
    def next_batch(self, number):
        xs = []
        ys = []
        for j in range(number):
            xs.append(self.data[self.crt])
            ys.append(self.result[self.crt])
            self.crt+=1
            if self.crt == 200:
                self.crt=0
        return xs, ys
    def next_batch_training(self, number):
        xs = []
        ys = []
        for j in range(number):
            elem = random.randint(200,270)
            xs.append(self.data[elem])
            ys.append(self.result[elem])
        return xs, ys
    def batch_testx(self):
        return self.data[200:]
    def batch_testy(self):
        return self.result[200:]
    def getx(self, i):
        return self.data[i]
    def gety(self, i):
        return self.result[i]