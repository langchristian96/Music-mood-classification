from __future__ import print_function

from ro.ubb.dataset import Dataset

dataset = Dataset()

import tensorflow as tf
# Parameters
learning_rate = 0.1
num_steps = 500
batch_size = 128
display_step = 100
from tensorflow.python.client import device_lib
print(device_lib.list_local_devices())
# Network Parameters
n_hidden_1 = 39 # 1st layer number of neurons
n_hidden_2 = 24 # 2nd layer number of neurons
n_hidden_3 = 15
num_input = 54
num_classes = 4

# tf Graph input
X = tf.placeholder("float", [None, num_input])
Y = tf.placeholder("float", [None, num_classes])
# Store layers weight & bias
weights = {
    'h1': tf.Variable(tf.random_normal([num_input, n_hidden_1])),
    'h2': tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2])),
    'h3': tf.Variable(tf.random_normal([n_hidden_2, n_hidden_3])),
    'out': tf.Variable(tf.random_normal([n_hidden_3, num_classes]))
}
biases = {
    'b1': tf.Variable(tf.random_normal([n_hidden_1])),
    'b2': tf.Variable(tf.random_normal([n_hidden_2])),
    'b3': tf.Variable(tf.random_normal([n_hidden_3])),
    'out': tf.Variable(tf.random_normal([num_classes]))
}
# Create model
def neural_net(x):
    # Hidden fully connected layer with 256 neurons
    layer_1 = tf.add(tf.matmul(x, weights['h1']), biases['b1'])
    # Hidden fully connected layer with 256 neurons
    layer_2 = tf.add(tf.matmul(layer_1, weights['h2']), biases['b2'])
    # Output fully connected layer with a neuron for each class
    layer_3 = tf.add(tf.matmul(layer_2, weights['h3']), biases['b3'])
    # Output fully connected layer with a neuron for each class
    out_layer = tf.matmul(layer_3, weights['out']) + biases['out']
    return out_layer

# Construct model
logits = neural_net(X)

# Define loss and optimizer
loss_op = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(
    logits=logits, labels=Y))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
train_op = optimizer.minimize(loss_op)

# Evaluate model (with test logits, for dropout to be disabled)
correct_pred = tf.equal(tf.argmax(logits, 1), tf.argmax(Y, 1))
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
pred=tf.argmax(logits, 1)
# Initialize the variables (i.e. assign their default value)
init = tf.global_variables_initializer()
# Start training
with tf.Session(config=tf.ConfigProto(log_device_placement=True)) as sess:

    # Run the initializer
    sess.run(init)

    for step in range(1, num_steps+1):
        batch_x, batch_y = dataset.next_batch(batch_size)
        # Run optimization op (backprop)
        sess.run(train_op, feed_dict={X: batch_x, Y: batch_y})
        if step % display_step == 0 or step == 1:
            # Calculate batch loss and accuracy
            loss, acc = sess.run([loss_op, accuracy], feed_dict={X: batch_x,
                                                                 Y: batch_y})
            print("Step " + str(step) + ", Minibatch Loss= " + \
                  "{:.4f}".format(loss) + ", Training Accuracy= " + \
                  "{:.3f}".format(acc))

    print("Optimization Finished!")

    # Calculate accuracy for test set
    print("Testing Accuracy:", \
        sess.run(accuracy, feed_dict={X: dataset.batch_testx(),
                                      Y: dataset.batch_testy()}))

    per_class_real = [0, 0, 0, 0]
    per_class_predicted = [0, 0, 0, 0]
    for e in range(300,744):
        result = sess.run(pred, feed_dict={X: [dataset.getx(e)]})
        # print(e)
        # print(dataset.gety(e).index(1))
        result=result[0]
        per_class_real[dataset.gety(e).index(1)]+=1
        if result == dataset.gety(e).index(1):
            per_class_predicted[result]+=1

    map = {0:"happy", 1:"angry", 2:"relaxed", 3:"sad"}
    print(per_class_predicted,per_class_real)
    for i in range(4):
        res=per_class_predicted[i]*100/per_class_real[i]
        print(map[i]," ",res,"%")