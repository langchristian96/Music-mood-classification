from ro.ubb.dataset import Dataset

dataset = Dataset()

import numpy as np
import tensorflow as tf


K = 4

Xtr, Ytr = dataset.next_batch(500)  # whole training set
Xte, Yte = dataset.batch_testx(), dataset.batch_testy()

# tf Graph Input
xtr = tf.placeholder("float", [None, 54])
ytr = tf.placeholder("float", [None, 4])
xte = tf.placeholder("float", [54])

# Euclidean Distance
distance = tf.negative(tf.sqrt(tf.reduce_sum(tf.square(tf.subtract(xtr, xte)), reduction_indices=1)))
# Prediction: Get min distance neighbors
values, indices = tf.nn.top_k(distance, k=K, sorted=False)

nearest_neighbors = []
for i in range(K):
    nearest_neighbors.append(tf.argmax(ytr[indices[i]], 0))

neighbors_tensor = tf.stack(nearest_neighbors)
y, idx, count = tf.unique_with_counts(neighbors_tensor)
pred = tf.slice(y, begin=[tf.argmax(count, 0)], size=tf.constant([1], dtype=tf.int64))[0]

accuracy = 0.

# Initializing the variables
init = tf.global_variables_initializer()
per_class_real = [0,0,0,0]
per_class_predicted = [0,0,0,0]

# Launch the graph
with tf.Session() as sess:
    sess.run(init)

    # loop over test data
    for i in range(len(Xte)):
        # Get nearest neighbor
        nn_index = sess.run(pred, feed_dict={xtr: Xtr, ytr: Ytr, xte: Xte[i]})
        # Get nearest neighbor class label and compare it to its true label
        print("Test", i, "Prediction:", nn_index,
             "True Class:", np.argmax(Yte[i]))
        #Calculate accuracy
        if np.argmax(Yte[i]) == nn_index:
            per_class_predicted[nn_index] += 1
        per_class_real[np.argmax(Yte[i])]+=1
        if nn_index == np.argmax(Yte[i]):
            accuracy += 1. / len(Xte)
    print("Done!")
    print("Accuracy:", accuracy)
    print(per_class_predicted,per_class_real)

