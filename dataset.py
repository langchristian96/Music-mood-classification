import random


class Dataset:
    data = []
    result =[]
    crt = 0
    def __init__(self):
        filepath = "C:\work\licenta\\ro\\ubb\\featuresclass.txt"
        with open(filepath, "r") as f:
            lineargs = f.readline().strip().split(",")
            while(len(lineargs)==55):
                line = []
                for i in range(len(lineargs)-1):
                    line.append(float(lineargs[i]))
                resline = self.hot(int(lineargs[len(lineargs)-1]))
                self.data.append(line)
                self.result.append(resline)
                lineargs = f.readline().strip().split(",")
        # print(self.data)
        #
        # print(self.result)

    def hot(self, i):
        res = [0, 0, 0, 0]
        res[i] = 1
        return res
    def next_batch(self, number):
        xs = []
        ys = []
        for j in range(number):
            xs.append(self.data[self.crt])
            ys.append(self.result[self.crt])
            self.crt+=1
            if self.crt == 600:
                self.crt=0
        return xs, ys
    def next_batch_training(self, number):
        xs = []
        ys = []
        for j in range(number):
            elem = random.randint(600,744)
            xs.append(self.data[elem])
            ys.append(self.result[elem])
        return xs, ys
    def batch_testx(self):
        return self.data[600:]
    def batch_testy(self):
        return self.result[600:]
    def getx(self, i):
        return self.data[i]
    def gety(self, i):
        return self.result[i]